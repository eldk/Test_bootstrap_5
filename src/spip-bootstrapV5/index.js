//tout bootstrap - ESM - https://github.com/twbs/bootstrap/pull/28386 - voir ../../node_modules/bootstrap/package.json "module": "dist/js/bootstrap.esm.js",
// + popper.js - ESM - ../../node_modules/popper.js/dist/esm/popper.js
import * as bootstrap from 'bootstrap';
// Supprimer les modules non utilisés. Si Popover, Tooltip, Dropdown sont utilisés : popper.js sera ajouté.
//import {Alert, Button, Carousel, Collapse, Dropdown, Modal, Popover, ScrollSpy, Tab, Toast, Tooltip} from "bootstrap"; //utilise ESM

//styles personnalisés
import '../../scss/custom.scss'; // Import our scss file

if (typeof bootstrap !== 'undefined' || bootstrap !== null) {
    console.log("Alert version : " + bootstrap.Alert.VERSION);
    console.log("Button version : " + bootstrap.Button.VERSION);
    console.log("Popover version : " + bootstrap.Popover.VERSION);
    console.log("Tooltip version : " + bootstrap.Tooltip.VERSION);
    window.bootstrap = bootstrap;
} else {
    console.log("index.js : Bootstrap non défini");
}
