Test Boostrap 5 Alpha/ Parcel

Pour installer, cloner le dépôt puis `npm install`

Pour tester : `npm run dev`, modifier src/spip-bootstrapV5/index.js, src/spip-bootstrapV5/index.html ou scss/custom.scss, http://127.0.0.1:1234

Pour construire le css et le javascript de Boostrap : `npm run build`. build/spip-bootstrapV5.xxxxxxxx.css build/spip-bootstrapV5.xxxxxxxx.js

configuration scss : scss/custom.scss

configuration modules javascript : src/spip-bootstrapV5/index.js


Mettre à jour Bootstrap avec les derniers commits du dépot twbs/bootstrap :

`npm uninstall bootstrap`

`npm install --save-dev twbs/bootstrap.git#main`

Consulter la documentation Bootstrap 5 (http://127.0.0.1:9001):

`cd node_modules/bootstrap/`

`npm install`

`npm run docs-serve`
